package gateway

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"

	gw "github.com/brocaar/chirpstack-api/go/v3/gw"
	paho "github.com/eclipse/paho.mqtt.golang"

	// nolint: staticcheck
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/revive/internal/config"
	"gitlab.com/wobcom/iot/revive/internal/mqtt"
	"gitlab.com/wobcom/iot/revive/internal/types"
)

func Run(cmd *cobra.Command, args []string) {
	// trap Ctrl+C and call cancel on the context
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-quit
		signal.Stop(quit)
		cancel()
	}()

	hostname, _ := os.Hostname()
	log.Info().Msgf("starting gateway executor on %s", hostname)

	opts := mqtt.NewConfig(config.C.MQTT)
	opts.SetOnConnectHandler(NewOnConnectHandler(config.C))
	client := paho.NewClient(opts)
	go mqtt.ConnectLoop(client, opts, ctx)

	<-ctx.Done()
	log.Info().Msg("client disconnecting")
	client.Disconnect(1000)
	log.Info().Msg("client disconnected")
}

func NewOnConnectHandler(conf config.Config) paho.OnConnectHandler {
	return func(c paho.Client) {
		log.Info().Msgf("connected to %s", conf.MQTT.Server)
		topic := mqtt.GetCommandTopic(types.GWID(conf.Gateway.ID))
		log.Info().Msgf("subscribing to command topic %s", topic)
		token := c.Subscribe(topic, 0, CommandMessageHandler)
		go func() {
			<-token.Done()
			if token.Error() != nil {
				log.Error().Err(token.Error()).Msgf("could not subscribe to %s", topic)
			}
		}()
	}
}

func CommandMessageHandler(c paho.Client, m paho.Message) {
	log.Debug().Msgf("received a message with the ID: %d", m.MessageID())
	go func() {
		var execFrame gw.GatewayCommandExecRequest
		err := proto.Unmarshal(m.Payload(), &execFrame)
		if err != nil {
			log.Error().Err(err).Send()
		}

		out, _ := json.MarshalIndent(execFrame, "", "  ")
		log.Info().RawJSON("payload", out).Send()
		log.Info().Msgf("list of commands %s", config.C.Gateway.Commands)
		command, ok := config.C.Gateway.Commands[execFrame.Command]
		if !ok {
			log.Error().Msgf("command %s not found", execFrame.Command)
			return
		}

		stdout, stderr, err := execute(&command, execFrame.Stdin, execFrame.Environment)
		if err != nil {
			log.Error().Err(err).Msgf("command %s", execFrame.Command)
		}
		resp := gw.GatewayCommandExecResponse{
			GatewayId: execFrame.GatewayId,
			ExecId:    execFrame.ExecId,
			Stdout:    stdout,
			Stderr:    stderr,
		}
		respOut, _ := json.MarshalIndent(resp, "", "  ")
		log.Info().Msgf("stdout %s", stdout)
		log.Info().RawJSON("resp", respOut).Send()
	}()
}

func execute(command *types.Command, stdin []byte, environment map[string]string) ([]byte, []byte, error) {

	cmdArgs, err := ParseCommandLine(command.Exec)
	if err != nil {
		return nil, nil, errors.Wrap(err, "parse command error")
	}
	if len(cmdArgs) == 0 {
		return nil, nil, errors.New("no command is given")
	}

	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(command.Timeout))
	defer cancel()

	cmdCtx := exec.CommandContext(ctx, cmdArgs[0], cmdArgs[1:]...)

	// The default is that when cmdCtx.Env is nil, os.Environ() are being used
	// automatically. As we want to add additional env. variables, we want to
	// extend this list, thus first need to set them to os.Environ()
	cmdCtx.Env = os.Environ()
	for k, v := range environment {
		cmdCtx.Env = append(cmdCtx.Env, fmt.Sprintf("%s=%s", k, v))
	}

	stdinPipe, err := cmdCtx.StdinPipe()
	if err != nil {
		return nil, nil, errors.Wrap(err, "get stdin pipe error")
	}

	stdoutPipe, err := cmdCtx.StdoutPipe()
	if err != nil {
		return nil, nil, errors.Wrap(err, "get stdout pipe error")
	}

	stderrPipe, err := cmdCtx.StderrPipe()
	if err != nil {
		return nil, nil, errors.Wrap(err, "get stderr pipe error")
	}

	go func() {
		defer stdinPipe.Close()
		if _, err := stdinPipe.Write(stdin); err != nil {
			log.Error().Err(err).Msg("commands: write to stdin error")
		}
	}()

	if err := cmdCtx.Start(); err != nil {
		return nil, nil, errors.Wrap(err, "starting command error")
	}

	stdoutB, _ := ioutil.ReadAll(stdoutPipe)
	stderrB, _ := ioutil.ReadAll(stderrPipe)

	if err := cmdCtx.Wait(); err != nil {
		return nil, nil, errors.Wrap(err, "waiting for command to finish error")
	}

	return stdoutB, stderrB, nil
}

// ParseCommandLine parses the given command to commands and arguments.
// source: https://stackoverflow.com/questions/34118732/parse-a-command-line-string-into-flags-and-arguments-in-golang
func ParseCommandLine(command string) ([]string, error) {
	var args []string
	state := "start"
	current := ""
	quote := "\""
	escapeNext := true
	for i := 0; i < len(command); i++ {
		c := command[i]

		if state == "quotes" {
			if string(c) != quote {
				current += string(c)
			} else {
				args = append(args, current)
				current = ""
				state = "start"
			}
			continue
		}

		if escapeNext {
			current += string(c)
			escapeNext = false
			continue
		}

		if c == '\\' {
			escapeNext = true
			continue
		}

		if c == '"' || c == '\'' {
			state = "quotes"
			quote = string(c)
			continue
		}

		if state == "arg" {
			if c == ' ' || c == '\t' {
				args = append(args, current)
				current = ""
				state = "start"
			} else {
				current += string(c)
			}
			continue
		}

		if c != ' ' && c != '\t' {
			state = "arg"
			current += string(c)
		}
	}

	if state == "quotes" {
		return []string{}, errors.New(fmt.Sprintf("Unclosed quote in command line: %s", command))
	}

	if current != "" {
		args = append(args, current)
	}

	return args, nil
}
