package types

import "time"

//go:generate mockgen --source=internal/interfaces/interfaces.go --destination=mocks/mock_interfaces.go --package=mocks

type GWID string

type Command struct {
	Exec    string        `mapstructure:"exec"`
	Timeout time.Duration `mapstructure:"timeout"`
}
