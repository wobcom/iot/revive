// Package cmd defines the command-line interface.
package cmd

import (
	"bytes"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/wobcom/iot/revive/internal/config"
)

var (
	cfgFile     string
	jsonLogging bool
)

var rootCmd = &cobra.Command{
	Use: "revive",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// Setting up logging.
		if !jsonLogging {
			log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		}
		log.Logger = log.With().Caller().Logger()
	},
	Run: func(cmd *cobra.Command, args []string) {
		// Doing nothing, only printing help.
		if err := cmd.Help(); err != nil {
			log.Fatal().Msg(err.Error())
		}
	},
}

func init() {
	cobra.OnInitialize(initConfig)

	// Config file.
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file")

	// Defaults
	viper.SetDefault("general.log_level", 4)
	viper.SetDefault("general.shutdown_timeout", "10s")
	viper.SetDefault("general.pprof", false)
	viper.SetDefault("general.json_logging", false)

	viper.SetDefault("mqtt.client_id", "")
	viper.SetDefault("mqtt.server", "tcp://localhost:1883")
	viper.SetDefault("mqtt.username", "")
	viper.SetDefault("mqtt.password", "")
	viper.SetDefault("mqtt.qos", 0)
	viper.SetDefault("mqtt.clean_session", true)
	viper.SetDefault("mqtt.keep_alive", 2*time.Second)
	viper.SetDefault("mqtt.max_reconnect_interval", 2*time.Second)

	// Version.
	rootCmd.AddCommand(versionCmd)

	initConfigCmd(rootCmd)

	// Alertmanager.
	initAlertManager(rootCmd)

	// Gateway Executor
	initGatewayCmd(rootCmd)
}

func initConfig() {
	if cfgFile != "" {
		b, err := ioutil.ReadFile(cfgFile)
		if err != nil {
			log.Error().Err(err).Str("config", cfgFile).Msg("error loading config file")
		}

		viper.SetConfigType("toml")

		if err := viper.ReadConfig(bytes.NewBuffer(b)); err != nil {
			log.Error().Err(err).Str("config", cfgFile).Msg("error loading config file")
		} else {
			log.Info().Msgf("Loaded config file: %s", viper.ConfigFileUsed())
		}
	}

	viper.SetEnvPrefix("REVIVE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	viper.AutomaticEnv()
	if err := viper.Unmarshal(&config.C); err != nil {
		log.Fatal().Err(err).Msg("unmarshal config error")
	}
}

func Execute() error {
	return rootCmd.Execute()
}
