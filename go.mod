module gitlab.com/wobcom/iot/revive

go 1.15

require (
	github.com/brocaar/chirpstack-api/go/v3 v3.9.3
	github.com/eclipse/paho.mqtt.golang v1.3.2
	github.com/golang/mock v1.5.0
	github.com/golang/protobuf v1.4.3
	github.com/pkg/errors v0.9.1
	github.com/prometheus/alertmanager v0.21.0
	github.com/prometheus/client_golang v1.9.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.xsfx.dev/logginghandler v0.0.4
	google.golang.org/protobuf v1.25.0 // indirect
)
