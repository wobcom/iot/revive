package alertmanager

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var metricsAlertsTriggered = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "revive_alerts_total",
		Help: "The total number of triggered alerts",
	},
)

//nolint:gochecknoglobals,exhaustivestruct
var metricsAlertsFailed = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "revive_alerts_failed",
		Help: "The total number of failed alert handling",
	},
)

//nolint:gochecknoglobals,exhaustivestruct
var metricsAlertsOK = promauto.NewCounter(
	prometheus.CounterOpts{
		Name: "revive_alerts_ok",
		Help: "The total number of ok alert handling",
	},
)
