# revive

`revive` is a custom alertmanager to revive a lorawan gateway through chirpstack.

## Sub Packages

* [cmd](./cmd): Package cmd defines the command-line interface.

* [mocks](./mocks): Package mocks is a generated GoMock package.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
