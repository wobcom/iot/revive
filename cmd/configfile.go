package cmd

import (
	"html/template"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/revive/internal/config"
)

const configTemplate = `[general]
# Log level
#
# debug=5, info=4, warning=3, error=2, fatal=1, panic=0
log_level={{ .General.LogLevel }}
shutdown_timeout="{{ .General.ShutdownTimeout}}"
pprof={{ .General.PPROF }}
json_logging={{ .General.JSONLogging}}
[alertmanager]
bind="{{ .AlertManager.Bind }}"
[mqtt]
# MQTT server (e.g. scheme://host:port where scheme is tcp, ssl or ws)
server="{{ .MQTT.Server }}"

# Connect with the given username (optional)
username="{{ .MQTT.Username }}"

# Connect with the given password (optional)
password="{{ .MQTT.Password }}"

# Maximum interval that will be waited between reconnection attempts when connection is lost.
# Valid units are 'ms', 's', 'm', 'h'. Note that these values can be combined, e.g. '24h30m15s'.
max_reconnect_interval="{{ .MQTT.MaxReconnectInterval }}"

# Quality of service level
#
# 0: at most once
# 1: at least once
# 2: exactly once
#
# Note: an increase of this value will decrease the performance.
# For more information: https://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels

qos={{ .MQTT.QOS }}
# Clean session
#
# Set the "clean session" flag in the connect message when this client
# connects to an MQTT broker. By setting this flag you are indicating
# that no messages saved by the broker for this client should be delivered.
clean_session={{ .MQTT.CleanSession }}

# Client ID
#
# Set the client id to be used by this client when connecting to the MQTT
# broker. A client id must be no longer than 23 characters. When left blank,
# a random id will be generated. This requires clean_session=true.
client_id="{{ .MQTT.ClientID }}"

keep_alive="{{ .MQTT.KeepAlive }}"

[gateway]
gateway_id="{{ .Gateway.ID }}"

[gateway.command.revive]
	exec="/opt/revive/revive.sh'"
	timeout="2s"
`

var configCmd = &cobra.Command{
	Use:   "configfile",
	Short: "Print the configuration file",
	RunE: func(cmd *cobra.Command, args []string) error {
		t := template.Must(template.New("config").Parse(configTemplate))
		err := t.Execute(os.Stdout, config.C)
		if err != nil {
			return errors.Wrap(err, "execute config template error")
		}

		return nil
	},
}

func initConfigCmd(rootCmd *cobra.Command) {

	rootCmd.AddCommand(configCmd)
}
