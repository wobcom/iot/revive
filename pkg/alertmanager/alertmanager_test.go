package alertmanager_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/prometheus/alertmanager/template"
	"github.com/rs/zerolog"
	"gitlab.com/wobcom/iot/revive/internal/types"
	"gitlab.com/wobcom/iot/revive/mocks"
	"gitlab.com/wobcom/iot/revive/pkg/alertmanager"

	"github.com/stretchr/testify/assert"
)

func TestMock(t *testing.T) {
	assert := assert.New(t)

	var wg sync.WaitGroup
	wg.Add(1)
	defer wg.Wait()

	ctrl := gomock.NewController(t)

	mockExecutor := mocks.NewMockExecutor(ctrl)
	mockExecutor.
		EXPECT().
		Exec(
			gomock.Any(),
			"revive",
			gomock.Any(),
		).
		Do(func(gwID types.GWID, cmd string, logger zerolog.Logger) {
			assert.Equal(types.GWID("ab12"), gwID)
			wg.Done()
		}).
		Return(nil)

	alert := []byte(`
{
    "status": "firing",
    "alerts": [
        {
            "status": "firing",
            "labels": {
                "id": "ab12"
            }
        }
    ]
}
    `)

	req, err := http.NewRequest("POST", "/", bytes.NewReader(alert))
	if err != nil {
		assert.NoError(err)
	}

	w := httptest.NewRecorder()
	h := alertmanager.Handler{Exec: mockExecutor}
	h.ServeHTTP(w, req)

	resp := w.Result()

	assert.Equal(http.StatusOK, resp.StatusCode)
}

func TestHandler(t *testing.T) {
	assert := assert.New(t)

	tables := []struct {
		method     string
		statusCode int
		input      template.Data
	}{
		{
			"GET",
			405,
			template.Data{},
		},
	}

	for _, table := range tables {
		i, err := json.Marshal(table.input)
		assert.NoError(err)

		req, err := http.NewRequest(table.method, "/", bytes.NewReader(i))
		if err != nil {
			assert.NoError(err)
		}

		w := httptest.NewRecorder()
		h := alertmanager.Handler{}
		h.ServeHTTP(w, req)

		resp := w.Result()

		assert.Equal(table.statusCode, resp.StatusCode)
	}
}
