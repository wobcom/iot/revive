package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/wobcom/iot/revive/internal/config"
	"gitlab.com/wobcom/iot/revive/pkg/alertmanager"
)

var alertmanagerCmd = &cobra.Command{
	Use:     "alertmanager",
	Short:   "Start alertmanager webhook",
	PreRunE: CheckUsernamePassword,
	Run:     alertmanager.Run,
}

func initAlertManager(rootCmd *cobra.Command) {
	alertmanagerCmd.Flags().StringVar(&config.C.AlertManager.Bind, "bind", "0.0.0.0:1971", "HTTP listen string")

	if err := viper.BindPFlag("alertmanager.bind", alertmanagerCmd.Flags().Lookup("bind")); err != nil {
		log.Fatal().Msg(err.Error())
	}

	viper.SetDefault("alertmanager.bind", "0.0.0.0:1971")

	rootCmd.AddCommand(alertmanagerCmd)
}
