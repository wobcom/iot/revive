{{- define "name" -}}
{{- default .Release.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "quotedName" -}}
{{- default .Release.Name .Values.nameOverride | trunc 63 | trimSuffix "-" | quote -}}
{{- end -}}

{{- define "labels" -}}
app.kubernetes.io/name: {{ template "name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.Version }}
app.kubernetes.io/managed-by: Helm
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{- end -}}

{{- define "selector" -}}
app.kubernetes.io/name: {{ template "name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: Helm
{{- end -}}
