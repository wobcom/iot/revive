package mqtt

import (
	"context"
	"fmt"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	paho "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/revive/internal/config"
	"gitlab.com/wobcom/iot/revive/internal/types"
)

func ConnectLoop(client paho.Client, opts *paho.ClientOptions, ctx context.Context) {
	log.Info().Msgf("connecting to %s", opts.Servers[0].String())

	for {
		select {
		case <-ctx.Done():
			return
		default:
			if token := client.Connect(); token.Wait() && token.Error() != nil {
				log.Error().Err(token.Error()).Msgf("connecting to mqtt broker failed, will retry in %s",
					opts.MaxReconnectInterval)
				time.Sleep(opts.MaxReconnectInterval)
			} else {
				return
			}
		}
	}
}

func NewConfig(conf config.MQTTConfig) *mqtt.ClientOptions {
	log.Debug().Msg("creating MQTT client configuration")

	opts := mqtt.NewClientOptions()

	if conf.Username != "" {
		opts.SetUsername(conf.Username)
	}

	if conf.Password != "" {
		opts.SetPassword(conf.Password)
	}

	opts.AddBroker(conf.Server)

	if conf.ClientID == "" {
		hostname, _ := os.Hostname()
		opts.SetClientID("revive_" + hostname)
	} else {
		opts.SetClientID(conf.ClientID)
	}

	opts.SetAutoReconnect(true)
	opts.SetKeepAlive(conf.KeepAlive)
	opts.SetMaxReconnectInterval(conf.MaxReconnectInterval)
	opts.SetOnConnectHandler(func(c mqtt.Client) {
		log.Info().Msgf("connected to %s", opts.Servers[0].String())
	})
	opts.SetConnectionLostHandler(func(c mqtt.Client, err error) {
		log.Error().Msgf("mqtt connection lost: %s", err.Error())
	})
	opts.SetReconnectingHandler(func(c mqtt.Client, options *mqtt.ClientOptions) {
		log.Info().Msg("mqtt reconnecting...")
	})
	return opts
}

func GetCommandTopic(gwID types.GWID) (topic string) {
	topic = fmt.Sprintf("gateway/%s/command/exec", gwID)
	return
}
