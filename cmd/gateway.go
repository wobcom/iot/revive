package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/wobcom/iot/revive/pkg/gateway"
)

var gatewayCmd = &cobra.Command{
	Use:     "gateway",
	Short:   "Start gateway receiver",
	PreRunE: CheckUsernamePassword,
	Run:     gateway.Run,
}

func initGatewayCmd(rootCmd *cobra.Command) {

	viper.SetDefault("gateway.id", "")

	rootCmd.AddCommand(gatewayCmd)
}
