// `revive` is a custom alertmanager to revive a lorawan gateway through chirpstack.
package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/revive/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}
