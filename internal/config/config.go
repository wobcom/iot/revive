package config

import (
	"time"

	"gitlab.com/wobcom/iot/revive/internal/types"
)

type Config struct {
	General struct {
		LogLevel        int           `mapstructure:"log_level"`
		ShutdownTimeout time.Duration `mapstructure:"shutdown_timeout"`
		PPROF           bool          `mapstructure:"pprof"`
		JSONLogging     bool          `mapstructure:"json_logging"`
	}
	MQTT         MQTTConfig         `mapstructure:"mqtt"`
	AlertManager AlertManagerConfig `mapstructure:"alertmanager"`
	Gateway      GatewayConfig      `mapstructure:"gateway"`
}

// C holds the global configuration.
var C Config

type MQTTConfig struct {
	Server               string        `mapstructure:"server"`
	Username             string        `mapstructure:"username"`
	Password             string        `mapstructure:"password"`
	MaxReconnectInterval time.Duration `mapstructure:"max_reconnect_interval"`
	KeepAlive            time.Duration `mapstructure:"keep_alive"`
	QOS                  uint8         `mapstructure:"qos"`
	CleanSession         bool          `mapstructure:"clean_session"`
	ClientID             string        `mapstructure:"client_id"`
}

type AlertManagerConfig struct {
	Bind string `mapstructure:"bind"`
}

type GatewayConfig struct {
	ID       string                   `mapstructure:"gateway_id"`
	Commands map[string]types.Command `mapstructure:"commands"`
}
