// nolint: gochecknoglobals, exhaustivestruct
package alertmanager

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/brocaar/chirpstack-api/go/v3/gw"
	paho "github.com/eclipse/paho.mqtt.golang"

	// nolint: staticcheck
	"github.com/golang/protobuf/proto"
	"github.com/prometheus/alertmanager/template"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/revive/internal/config"
	"gitlab.com/wobcom/iot/revive/internal/interfaces"
	"gitlab.com/wobcom/iot/revive/internal/mqtt"
	"gitlab.com/wobcom/iot/revive/internal/types"
	"go.xsfx.dev/logginghandler"
)

// exec executor implemention.
type executor struct {
	Client paho.Client
}

func (e executor) Exec(gwID types.GWID, cmd string, logger zerolog.Logger) error {
	topic := mqtt.GetCommandTopic(gwID)

	// Setup logger.
	logger = logger.With().Str("gateway", string(gwID)).Str("topic", topic).Logger()

	// Create object to create right payload.
	// nolint: exhaustivestruct
	cmdExecReq := gw.GatewayCommandExecRequest{
		GatewayId: []byte(gwID),
		Command:   cmd,
	}

	// Marshal payload to protobuf bytes.
	out, err := proto.Marshal(&cmdExecReq)
	if err != nil {
		return fmt.Errorf("could not marshal: %w", err)
	}

	// Publish over MQTT.
	logger.Info().Msg("publish message")

	if token := e.Client.Publish(topic, 0, false, out); token.Wait() && token.Error() != nil {
		return fmt.Errorf("could not publish message: %w", token.Error())
	}

	logger.Info().Msg("published")
	metricsAlertsOK.Inc()

	return nil
}

// handler handles http requests.
type Handler struct {
	Exec interfaces.Executor
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Count the triggers.
	metricsAlertsTriggered.Inc()

	// The variable to store the parsed alerts.
	var alerts template.Data

	logger := logginghandler.Logger(r)

	// Check if its HTTP POST. The only allowed method here.
	if r.Method != http.MethodPost {
		logger.Error().Msgf("wrong method: %s", r.Method)
		http.Error(w, "wrong method", http.StatusMethodNotAllowed)
		metricsAlertsFailed.Inc()

		return
	}

	// Parse the alerts.
	if err := json.NewDecoder(r.Body).Decode(&alerts); err != nil {
		logger.Error().Msgf("could not decode alerts: %s", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		metricsAlertsFailed.Inc()

		return
	}

	// Going through the alerts.
	for _, alert := range alerts.Alerts.Firing() {
		// Working through the alerts in separated goroutines.
		// This should be ok!
		go func(a template.Alert) {
			// Running one goroutine per alert.
			logger.Info().Msg("got firing alert")

			// Only going further if there is a label with the gateway ID.
			id, ok := a.Labels["id"]
			if !ok {
				logger.Error().Msg("could not find id label in alert")

				return
			}

			// Publishing the MQTT command.
			if err := h.Exec.Exec(types.GWID(id), "revive", logger); err != nil {
				logger.Error().Msgf("could not exec: %s", err.Error())

				return
			}
		}(alert)
	}
}

func Run(cmd *cobra.Command, args []string) {
	log.Info().Msg("starting alertmanager webhook")

	// trap Ctrl+C and call cancel on the context
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-quit
		signal.Stop(quit)
		cancel()
	}()

	opts := mqtt.NewConfig(config.C.MQTT)
	client := paho.NewClient(opts)
	go mqtt.ConnectLoop(client, opts, ctx)

	e := executor{client}
	webhook := NewWebhook(e, config.C)
	// Start the server.
	go func() {
		log.Info().Msgf("srv listening on %s", config.C.AlertManager.Bind)
		if err := webhook.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("webhook start failed")
		}
	}()

	<-ctx.Done()

	log.Info().Msg("webhook shutting down")
	ctx, cancel = context.WithTimeout(context.Background(), config.C.General.ShutdownTimeout)
	defer cancel()

	client.Disconnect(uint(config.C.General.ShutdownTimeout / time.Millisecond))
	if err := webhook.Shutdown(ctx); err != nil {
		log.Fatal().Err(err).Msg("webhook shutdown failed")
	}
	log.Info().Msg("webhook shutdown properly")
}

func NewWebhook(e executor, conf config.Config) *http.Server {

	// HTTP server.
	r := http.NewServeMux()

	r.Handle("/", &Handler{Exec: e})
	r.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		if e.Client.IsConnected() {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusBadGateway)
		}
	})
	r.Handle("/metrics", promhttp.Handler())

	// PPROF can be enabled.
	if conf.General.PPROF {
		log.Debug().Msg("enabling PPROF endpoint")
		r.HandleFunc("/debug/pprof/", pprof.Index)
		r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		r.HandleFunc("/debug/pprof/profile", pprof.Profile)
		r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		r.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}

	return &http.Server{
		Addr:    conf.AlertManager.Bind,
		Handler: logginghandler.Handler(r),
	}
}
