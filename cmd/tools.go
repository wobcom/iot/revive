package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// nolint: goerr113
func CheckUsernamePassword(cmd *cobra.Command, args []string) error {
	if viper.GetString("mqtt.server") == "" {
		return fmt.Errorf("needs an MQTT server address")
	}

	username := viper.GetString("mqtt.username")
	password := viper.GetString("mqtt.password")

	if (username != "" && password == "") || (password != "" && username == "") {
		return fmt.Errorf("needs an MQTT username and password")
	}

	return nil
}
