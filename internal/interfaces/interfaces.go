package interfaces

import (
	"github.com/rs/zerolog"
	"gitlab.com/wobcom/iot/revive/internal/types"
)

// Executor is a interface to send exec commands to the gateway.
type Executor interface {
	Exec(gwID types.GWID, cmd string, logger zerolog.Logger) error
}
